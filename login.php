<?php
include 'connection.php';
ob_start();
?>
 <?php

session_start();
if(isset($_POST['b1']))
{
    $t1=$_POST['t1'];
    $t2=$_POST['t2'];
$log=mysqli_query($dbcon,"select * from user_log where uid='$t1' and pwd='$t2'");
if(mysqli_num_rows($log)>0)
{
$r=mysqli_fetch_row($log);
if($r[3]=="admin")
{
    $_SESSION['uid']=$t1;
	echo '<script>alert("Successfully Logged In !!")</script>';
	echo '<script>window.location.replace("./admin/home.php")</script>';
    
}
if($r[3]=="usr")
{
    $_SESSION['uid']=$t1;
	echo '<script>alert("Successfully Logged In !!")</script>';
	echo '<script>window.location.replace("./user/home.php")</script>';
}
if($r[3]=="user")
{
    $_SESSION['uid']=$t1;
	echo '<script>alert("Successfully Logged In !!")</script>';
	echo '<script>window.location.replace("./user/home.php")</script>';
}
}
else
{
    echo '<script>alert("Incorrect Username/Password")</script>'; 
}
    

}
                                    ?>

<!DOCTYPE html>
<html lang="zxx" class="no-js">

<head>
	<!-- Mobile Specific Meta -->
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!-- Favicon-->
	<link rel="shortcut icon" href="temp/img/fav.png">
	<!-- Author Meta -->
	<meta name="author" content="CodePixar">
	<!-- Meta Description -->
	<meta name="description" content="">
	<!-- Meta Keyword -->
	<meta name="keywords" content="">
	<!-- meta character set -->
	<meta charset="UTF-8">
	<!-- Site Title -->
	<title><?php echo $title ?></title>

	<!--
		CSS
		============================================= -->
	<link rel="stylesheet" href="temp/css/linearicons.css">
	<link rel="stylesheet" href="temp/css/owl.carousel.css">
	<link rel="stylesheet" href="temp/css/themify-icons.css">
	<link rel="stylesheet" href="temp/css/font-awesome.min.css">
	<link rel="stylesheet" href="temp/css/nice-select.css">
	<link rel="stylesheet" href="temp/css/nouislider.min.css">
	<link rel="stylesheet" href="temp/css/bootstrap.css">
	<link rel="stylesheet" href="temp/css/main.css">
</head>

<body>

	<!-- Start Header Area -->
	<?php
        
        include 'menu.php';
        
        ?>
	<!-- End Header Area -->

	<!-- Start Banner Area -->
	<section class="banner-area organic-breadcrumb">
		<div class="container">
			<div class="breadcrumb-banner d-flex flex-wrap align-items-center justify-content-end">
				<div class="col-first">
					<h1>Login/Register</h1>
					
				</div>
			</div>
		</div>
	</section>
	<!-- End Banner Area -->

	<!-- function for showing forgot password message -->
	<script>
		function showForgotPassword() {
			alert("Contact website admin for help.");
		}
	</script>

	<!--================Login Box Area =================-->
	<section class="login_box_area section_gap">
		<div class="container">
			<div class="row">
				<div class="col-lg-6">
					<div class="login_box_img">
						<img class="img-fluid" src="temp/img/login.jpg" alt="">
						<div class="hover">
							<h4>New to our website?</h4>
							<p>Create an account first to browse or add study materials and study equipment and make enquiries.</p>
                                                        <a class="primary-btn" href="reg.php">Create an Account</a>
						</div>
					</div>
				</div>
				<div class="col-lg-6">
					<div class="login_form_inner">
						<h3>Log in to enter</h3>
						<form class="row login_form"  method="post" id="contactForm">
							<div class="col-md-12 form-group">
								<input type="text" class="form-control" id="name" name="t1" placeholder="Email" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Email'" required>
							</div>
							<div class="col-md-12 form-group">
                                                            <input type="password" class="form-control" id="name" name="t2" placeholder="Password" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Password'" required>
							</div>
							<div class="col-md-12 form-group">
								<div class="creat_account">
									<input type="checkbox" id="f-option2" name="selector">
									<label for="f-option2">Keep me logged in</label>
								</div>
							</div>
							<div class="col-md-12 form-group">
								<button type="submit"name="b1" value="submit" class="primary-btn">Log In</button>
								<a href="#" onclick="showForgotPassword()">Forgot Password?</a>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--================End Login Box Area =================-->

	<!-- start footer Area -->
	<footer class="footer-area section_gap">
		<div class="container">
			
			<div class="footer-bottom d-flex justify-content-center align-items-center flex-wrap">
				<p class="footer-text m-0"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This website is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="#">Study Swap</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
</p>
			</div>
		</div>
	</footer>
	<!-- End footer Area -->


	<script src="temp/js/vendor/jquery-2.2.4.min.js"></script>
	<script src="temp/https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4"
	 crossorigin="anonymous"></script>
	<script src="temp/js/vendor/bootstrap.min.js"></script>
	<script src="temp/js/jquery.ajaxchimp.min.js"></script>
	<script src="temp/js/jquery.nice-select.min.js"></script>
	<script src="temp/js/jquery.sticky.js"></script>
	<script src="temp/js/nouislider.min.js"></script>
	<script src="temp/js/jquery.magnific-popup.min.js"></script>
	<script src="temp/js/owl.carousel.min.js"></script>
	<!--gmaps Js-->
	<script src="temp/https://maps.googleapis.com/maps/api/js?key=AIzaSyCjCGmQ0Uq4exrzdcL6rvxywDDOvfAu6eE"></script>
	<script src="temp/js/gmaps.min.js"></script>
	<script src="temp/js/main.js"></script>
</body>

</html>