<header class="header_area sticky-header">
		<div class="main_menu">
			<nav class="navbar navbar-expand-lg navbar-light main_box">
				<div class="container">
					<!-- Brand and toggle get grouped for better mobile display -->
                                        <h2><span style="color: darkorange" class="fa fa-book"></span> <?php echo $title ?></h2>
					<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
					 aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="collapse navbar-collapse offset" id="navbarSupportedContent">
						<ul class="nav navbar-nav menu_nav ml-auto">
                                                    <li class="nav-item"><a class="nav-link" href="home.php">Home</a></li>
							
							
							
                                                    <li class="nav-item"><a class="nav-link" href="add_study1.php">Add Study Material</a></li>

                                                    <li class="nav-item"><a class="nav-link" href="add_study2.php">Add Study Equipments</a></li>

<li class="nav-item submenu dropdown">
								<a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
								 aria-expanded="false">Search</a>
								<ul class="dropdown-menu">
									<li class="nav-item"><a class="nav-link" href="study1.php">Study Material</a></li>
<li class="nav-item"><a class="nav-link" href="study1_q.php">Quick Search</a></li>
<li class="nav-item"><a class="nav-link" href="study2.php">Study Equipments</a></li>
									</ul>
							</li>
<li class="nav-item submenu dropdown">
								<a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
								 aria-expanded="false">Enquiry</a>
								<ul class="dropdown-menu">
                                                                    <li class="nav-item"><a class="nav-link" href="enq1.php">Study Material</a></li>
                                                                    <li class="nav-item"><a class="nav-link" href="enq2.php">Study Equipments</a></li>
									</ul>
							</li>
<li class="nav-item"><a class="nav-link" href="../logout.php">Logout</a></li>
						</ul>
						<ul class="nav navbar-nav navbar-right">
							<li class="nav-item"><a href="#" class="cart"><span class="fa fa-diamond"></span></a></li>
							
					</div>
				</div>
			</nav>
		</div>
		
	</header>