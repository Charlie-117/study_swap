<?php
include '../connection.php';
ob_start();
session_start();
$usr=$_SESSION['uid'];

?>
<?php

if($usr=$_SESSION['uid'])
{
    
}
 else
     {
    header("location:../index.php");    
}
?>
<!DOCTYPE html>
<html lang="zxx" class="no-js">

<head>
    <!-- Mobile Specific Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Favicon-->
    <link rel="shortcut icon" href="../temp/img/fav.png">
    <!-- Author Meta -->
    <meta name="author" content="CodePixar">
    <!-- Meta Description -->
    <meta name="description" content="">
    <!-- Meta Keyword -->
    <meta name="keywords" content="">
    <!-- meta character set -->
    <meta charset="UTF-8">
    <!-- Site Title -->
    <title><?php echo $title ?></title>

    <!--
            CSS
            ============================================= -->
    <link rel="stylesheet" href="../temp/css/linearicons.css">
    <link rel="stylesheet" href="../temp/css/owl.carousel.css">
    <link rel="stylesheet" href="../temp/css/font-awesome.min.css">
    <link rel="stylesheet" href="../temp/css/themify-icons.css">
    <link rel="stylesheet" href="../temp/css/nice-select.css">
    <link rel="stylesheet" href="../temp/css/nouislider.min.css">
    <link rel="stylesheet" href="../temp/css/bootstrap.css">
    <link rel="stylesheet" href="../temp/css/main.css">
</head>

<body>

    <!-- Start Header Area -->
	<?php
        
        include 'menu.php';
        ?>
	<!-- End Header Area -->

    <!-- Start Banner Area -->
    <section class="banner-area organic-breadcrumb">
        <div class="container">
            <div class="breadcrumb-banner d-flex flex-wrap align-items-center justify-content-end">
                <div class="col-first">
                    <h1>Study Equipment Enquiry</h1>
                    
                </div>
            </div>
        </div>
    </section>
    <!-- End Banner Area -->

    <!--================Cart Area =================-->
    <section class="cart_area">
        <div class="">
            <div class="cart_inner">
                <div class="table-responsive">
                     <?php
                        
                                                        $sel_gal=mysqli_query($dbcon,"select * from enq2 where sid='$usr' order by id desc");
                                                        if(mysqli_num_rows($sel_gal)>0)
                                                        {$i=0;
                                                        ?>
                    <table class="table">
                        <thead>
                            
                            
                            <tr>
                                <th scope="col">Equipment</th>
                                <th scope="col">Price</th>
                                <th scope="col">from</th>
                                <th scope="col">Contact</th>
                                <th scope="col">Address</th>
                                <th scope="col">Message</th>
                                <th scope="col">Date</th>
                            </tr>
                            
                            
                        </thead>
                        <tbody>
                            <?php
                            while($r_gal=mysqli_fetch_row($sel_gal))
                                                            {
                                $sel=mysqli_query($dbcon,"select * from study2 where id='$r_gal[6]'");
    $r=mysqli_fetch_row($sel);
                            ?>
                            <tr>
                                <td>
                                    <div class="media">
                                        <div class="d-flex">
                                            
                                            <img style="width: 70px;height: 80px" src="../img3/<?php echo $r[4] ?>" alt="">
                                      
                                            
                                        </div>
                                        <div class="media-body">
                                            <p><?php echo $r[1] ?></p>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <h5><?php echo $r[3] ?> Rs/-</h5>
                                </td>
                                <td><?php echo $r_gal[1] ?></td>
                                <td>
                                    <a href="tel:<?php echo $r_gal[3] ?>"> <?php echo $r_gal[3] ?> <span class="fa fa-phone"></span></a>
                                </td>
                                <td><?php echo $r_gal[4] ?></td>
                                <td><?php echo $r_gal[5] ?></td>
                                <td><?php echo $r_gal[9] ?></td>
                            </tr>
                            
                            <?php
                                                            }
                                                            ?>
                            
                            
                            
                            
                        </tbody>
                    </table>
                    <?php
                                                        }
                                                        else {
                                                        ?>
                    <table class="table">
                        <thead>
                            
                            
                            <tr>
                                <th scope="col">Equipment</th>
                                <th scope="col">Price</th>
                                <th scope="col">from</th>
                                <th scope="col">Contact</th>
                                <th scope="col">Address</th>
                                <th scope="col">Message</th>
                                <th scope="col">Date</th>
                            </tr>
                            
                            
                        </thead>
                        <tbody>
                            <td align="center" colspan="7">
                            No enquiries found.
                            </td>
                        </tbody>
                    </table>
                    <?php
                                                        }
                                                        ?>
                </div>
            </div>
        </div>
    </section>
    <!--================End Cart Area =================-->

    <!-- start footer Area -->
    <footer class="footer-area section_gap">
        <div class="container">
            
            <div class="footer-bottom d-flex justify-content-center align-items-center flex-wrap">
                <p class="footer-text m-0"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This website is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="#">Study Swap</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
</p>
            </div>
        </div>
    </footer>
    <!-- End footer Area -->

    <script src="../temp/js/vendor/jquery-2.2.4.min.js"></script>
	<script src="../temp/https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4"
	 crossorigin="anonymous"></script>
	<script src="../temp/js/vendor/bootstrap.min.js"></script>
	<script src="../temp/js/jquery.ajaxchimp.min.js"></script>
	<script src="../temp/js/jquery.nice-select.min.js"></script>
	<script src="../temp/js/jquery.sticky.js"></script>
    <script src="../temp/js/nouislider.min.js"></script>
	<script src="../temp/js/jquery.magnific-popup.min.js"></script>
	<script src="../temp/js/owl.carousel.min.js"></script>
	<!--gmaps Js-->
	<script src="../temp/https://maps.googleapis.com/maps/api/js?key=AIzaSyCjCGmQ0Uq4exrzdcL6rvxywDDOvfAu6eE"></script>
	<script src="../temp/js/gmaps.min.js"></script>
	<script src="../temp/js/main.js"></script>
</body>

</html>