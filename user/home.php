<?php
include '../connection.php';
ob_start();
session_start();
$usr=$_SESSION['uid'];
$usr_query=mysqli_query($dbcon,"SELECT * FROM user_data WHERE em='" . $usr . "'");
$usr_arr = mysqli_fetch_row($usr_query);
?>
<!DOCTYPE html>
<html lang="zxx" class="no-js">

<head>
	<!-- Mobile Specific Meta -->
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!-- Favicon-->
	<link rel="shortcut icon" href="../temp/img/fav.png">
	<!-- Author Meta -->
	<meta name="author" content="CodePixar">
	<!-- Meta Description -->
	<meta name="description" content="">
	<!-- Meta Keyword -->
	<meta name="keywords" content="">
	<!-- meta character set -->
	<meta charset="UTF-8">
	<!-- Site Title -->
	<title><?php echo $title ?></title>
	<!--
		CSS
		============================================= -->
	<link rel="stylesheet" href="../temp/css/linearicons.css">
	<link rel="stylesheet" href="../temp/css/font-awesome.min.css">
	<link rel="stylesheet" href="../temp/css/themify-icons.css">
	<link rel="stylesheet" href="../temp/css/bootstrap.css">
	<link rel="stylesheet" href="../temp/css/owl.carousel.css">
	<link rel="stylesheet" href="../temp/css/nice-select.css">
	<link rel="stylesheet" href="../temp/css/nouislider.min.css">
	<link rel="stylesheet" href="../temp/css/ion.rangeSlider.css" />
	<link rel="stylesheet" href="../temp/css/ion.rangeSlider.skinFlat.css" />
	<link rel="stylesheet" href="../temp/css/magnific-popup.css">
	<link rel="stylesheet" href="../temp/css/main.css">
</head>

<body>

	<!-- Start Header Area -->
	<?php
        
        include 'menu.php';
        
        ?>
	<!-- End Header Area -->

	<!-- start banner Area -->
	<section class="banner-area">
		<div class="container">
			<div class="row fullscreen align-items-center justify-content-start">
				<div class="col-lg-12">
					<div class="active-banner-slider owl-carousel">
						<!-- single-slide -->
						<div class="row single-slide align-items-center d-flex">
							<div class="col-lg-5 col-md-6">
								<div class="banner-content">
									<h1>Welcome <?php echo $usr_arr[1]; ?> to <br>STUDY SWAP</h1>
									<p>The study materials include lectures, notes, chapters, questions and answers, video lessons, exercise programs, and other documents. It also includes the learning pathways from different sources such as online sources and asking friends or taking the help of any third person.</p>
									
								</div>
							</div>
							<div class="col-lg-7">
								<div class="banner-img">
                                                                    <br/><br/>
                                                                    <img class="img-fluid" src="../temp/img/study-material-3327976-2793945.webp" alt="">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	

	<script src="../temp/js/vendor/jquery-2.2.4.min.js"></script>
	<script src="../temp/https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4"
	 crossorigin="anonymous"></script>
	<script src="../temp/js/vendor/bootstrap.min.js"></script>
	<script src="../temp/js/jquery.ajaxchimp.min.js"></script>
	<script src="../temp/js/jquery.nice-select.min.js"></script>
	<script src="../temp/js/jquery.sticky.js"></script>
	<script src="../temp/js/nouislider.min.js"></script>
	<script src="../temp/js/countdown.js"></script>
	<script src="../temp/js/jquery.magnific-popup.min.js"></script>
	<!--<script src="../temp/js/owl.carousel.min.js"></script>-->
	<!--gmaps Js-->
	<script src="../temp/https://maps.googleapis.com/maps/api/js?key=AIzaSyCjCGmQ0Uq4exrzdcL6rvxywDDOvfAu6eE"></script>
	<script src="../temp/js/gmaps.min.js"></script>
	<script src="../temp/js/main.js"></script>
</body>

</html>