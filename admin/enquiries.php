<?php
include '../connection.php';
ob_start();
session_start();
$usr=$_SESSION['uid'];

?>
<?php

if($usr=$_SESSION['uid'])
{
    
}
 else
     {
    header("location:../index.php");    
}
?>

<!DOCTYPE html>
<html lang="zxx" class="no-js">

<head>
    <!-- Mobile Specific Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Favicon-->
    <link rel="shortcut icon" href="../temp/img/fav.png">
    <!-- Author Meta -->
    <meta name="author" content="CodePixar">
    <!-- Meta Description -->
    <meta name="description" content="">
    <!-- Meta Keyword -->
    <meta name="keywords" content="">
    <!-- meta character set -->
    <meta charset="UTF-8">
    <!-- Site Title -->
    <title><?php echo $title ?></title>

    <!--
            CSS
            ============================================= -->
    <link rel="stylesheet" href="../temp/css/linearicons.css">
    <link rel="stylesheet" href="../temp/css/owl.carousel.css">
    <link rel="stylesheet" href="../temp/css/font-awesome.min.css">
    <link rel="stylesheet" href="../temp/css/themify-icons.css">
    <link rel="stylesheet" href="../temp/css/nice-select.css">
    <link rel="stylesheet" href="../temp/css/nouislider.min.css">
    <link rel="stylesheet" href="../temp/css/bootstrap.css">
    <link rel="stylesheet" href="../temp/css/main.css">
</head>

<body>

    <!-- Start Header Area -->
	<?php
        
        include 'menu.php';
        ?>
	<!-- End Header Area -->

    <!-- Start Banner Area -->
    <section class="banner-area organic-breadcrumb">
        <div class="container">
            <div class="breadcrumb-banner d-flex flex-wrap align-items-center justify-content-end">
                <div class="col-first">
                    <h1>View enquiries</h1>
                    
                </div>
            </div>
        </div>
    </section>
    <!-- End Banner Area -->

    <!--================Tracking Box Area =================-->
    <section class="tracking_box_area section_gap">
        <div class="container">
                
                    <h1>Study materials enquiry</h1>
                    <div class="col-lg-12">
                        <?php
                        
                                                        $sel_gal=mysqli_query($dbcon,"select * from enq1");
                                                        if(mysqli_num_rows($sel_gal)>0)
                                                        {$i=0;
                                                        ?>
                                    <table class="table table-bordered">
                                    <thead>
                                        
                                        <tr>
                                            <th scope="col">#</th>
                                            
                                            <th scope="col">Sender's email</th>
                                            <th scope="col">Receiver's email</th>
                                            <th scope="col">Product</th>
                                            <th scope="col">Message</th>

                                        </tr>
                                       
                                    </thead>
                                    
                                    <tbody>
                                         <?php
                                                            while($r_gal=mysqli_fetch_row($sel_gal))
                                                            {
                                                                $i++;
                                                                ?>
                                        <tr>
                                            <th scope="row"><?php echo $i ?></th>
                                            
                                            <td><?php echo $r_gal[2] ?></td>
                                            <td><?php echo $r_gal[8] ?></td>
                                            <td><a href="study_dt1.php?mid=<?php echo $r_gal[6] ?>"><span style="color: blue" class="fa fa-eye"></span></a></td>
                                            <td><?php echo $r_gal[5] ?></td>
                                            
                                        </tr>
                                        <?php
                                        
                                        
                                                            }
                                                            ?>
                                    </tbody>
                                </table>
                                    <?php
                                                        }
                                                        ?>
<br><br><br>
<h1>Study equipments enquiry</h1>
                    <div class="col-lg-12">
                        <?php
                        
                                                        $sel_gal=mysqli_query($dbcon,"select * from enq2");
                                                        if(mysqli_num_rows($sel_gal)>0)
                                                        {$i=0;
                                                        ?>
                                    <table class="table table-bordered">
                                    <thead>
                                        
                                        <tr>
                                            <th scope="col">#</th>
                                            
                                            <th scope="col">Sender's email</th>
                                            <th scope="col">Receiver's email</th>
                                            <th scope="col">Product</th>
                                            <th scope="col">Message</th>

                                        </tr>
                                       
                                    </thead>
                                    
                                    <tbody>
                                         <?php
                                                            while($r_gal=mysqli_fetch_row($sel_gal))
                                                            {
                                                                $i++;
                                                                ?>
                                        <tr>
                                            <th scope="row"><?php echo $i ?></th>
                                            
                                            <td><?php echo $r_gal[2] ?></td>
                                            <td><?php echo $r_gal[8] ?></td>
                                            <td><a href="study_dt2.php?mid=<?php echo $r_gal[6] ?>"><span style="color: blue" class="fa fa-eye"></span></a></td>
                                            <td><?php echo $r_gal[5] ?></td>
                                            
                                        </tr>
                                        <?php
                                        
                                        
                                                            }
                                                            ?>
                                    </tbody>
                                </table>
                                    <?php
                                                        }
                                                        ?>
                        
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--================End Tracking Box Area =================-->

    <!-- start footer Area -->
    <footer class="footer-area section_gap">
        <div class="container">
           
            <div class="footer-bottom d-flex justify-content-center align-items-center flex-wrap">
                <p class="footer-text m-0"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This website is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="#">Study Swap</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
</p>
            </div>
        </div>
    </footer>
    <!-- End footer Area -->




    <script src="../temp/js/vendor/jquery-2.2.4.min.js"></script>
	<script src="../temp/https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4"
	 crossorigin="anonymous"></script>
	<script src="../temp/js/vendor/bootstrap.min.js"></script>
	<script src="../temp/js/jquery.ajaxchimp.min.js"></script>
	<script src="../temp/js/jquery.nice-select.min.js"></script>
	<script src="../temp/js/jquery.sticky.js"></script>
    <script src="../temp/js/nouislider.min.js"></script>
	<script src="../temp/js/jquery.magnific-popup.min.js"></script>
	<script src="../temp/js/owl.carousel.min.js"></script>
	<!--gmaps Js-->
	<script src="../temp/https://maps.googleapis.com/maps/api/js?key=AIzaSyCjCGmQ0Uq4exrzdcL6rvxywDDOvfAu6eE"></script>
	<script src="../temp/js/gmaps.min.js"></script>
	<script src="../temp/js/main.js"></script>
</body>

</html>