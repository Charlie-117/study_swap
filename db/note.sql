-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 27, 2023 at 07:41 AM
-- Server version: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `note`
--

-- --------------------------------------------------------

--
-- Table structure for table `cate1`
--

CREATE TABLE IF NOT EXISTS `cate1` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nme` varchar(150) NOT NULL,
  `st` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `cate1`
--

INSERT INTO `cate1` (`id`, `nme`, `st`) VALUES
(1, '+1&+2', 0),
(2, 'BSC', 0),
(3, 'MSC', 0),
(4, 'BTECH', 0),
(5, 'MTECH', 0);

-- --------------------------------------------------------

--
-- Table structure for table `cate2`
--

CREATE TABLE IF NOT EXISTS `cate2` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mid` varchar(150) NOT NULL,
  `nme` varchar(150) NOT NULL,
  `st` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `cate2`
--

INSERT INTO `cate2` (`id`, `mid`, `nme`, `st`) VALUES
(1, '2', 'Computer Science', 0),
(2, '2', 'Mechanical', 0),
(3, '2', 'Civil', 0);

-- --------------------------------------------------------

--
-- Table structure for table `cate3`
--

CREATE TABLE IF NOT EXISTS `cate3` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mid` varchar(150) NOT NULL,
  `nme` varchar(150) NOT NULL,
  `st` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `cate3`
--

INSERT INTO `cate3` (`id`, `mid`, `nme`, `st`) VALUES
(1, '1', 'S1&S2', 0),
(2, '1', 'S3', 0),
(3, '1', 'S4', 0),
(4, '1', 'S5', 0),
(5, '1', 'S6', 0);

-- --------------------------------------------------------

--
-- Table structure for table `enq1`
--

CREATE TABLE IF NOT EXISTS `enq1` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nme` varchar(150) NOT NULL,
  `em` varchar(150) NOT NULL,
  `cont` varchar(150) NOT NULL,
  `addr` text NOT NULL,
  `msg` text NOT NULL,
  `mid` varchar(150) NOT NULL,
  `sid` varchar(150) NOT NULL,
  `uid` varchar(150) NOT NULL,
  `dt` date NOT NULL,
  `st` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `enq1`
--

INSERT INTO `enq1` (`id`, `nme`, `em`, `cont`, `addr`, `msg`, `mid`, `sid`, `uid`, `dt`, `st`) VALUES
(1, 'Ajay Kumar', 'akkk@gmail.com', '7894563215', 'Manathala Trivandrum Kerala', 'Does it has everything for studing Network', '3', 'ma@gmail.com', 'ma@gmail.com', '2023-02-21', 0);

-- --------------------------------------------------------

--
-- Table structure for table `enq2`
--

CREATE TABLE IF NOT EXISTS `enq2` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nme` varchar(150) NOT NULL,
  `em` varchar(150) NOT NULL,
  `cont` varchar(150) NOT NULL,
  `addr` text NOT NULL,
  `msg` text NOT NULL,
  `mid` varchar(150) NOT NULL,
  `sid` varchar(150) NOT NULL,
  `uid` varchar(150) NOT NULL,
  `dt` date NOT NULL,
  `st` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `enq2`
--

INSERT INTO `enq2` (`id`, `nme`, `em`, `cont`, `addr`, `msg`, `mid`, `sid`, `uid`, `dt`, `st`) VALUES
(1, 'Ravi Krishna', 'rk@gmail.com', '7895463215', 'Manathala Trivandrum Kerala', 'Is it in good working Condition??', '3', 'ma@gmail.com', 'ma@gmail.com', '2023-02-22', 0);

-- --------------------------------------------------------

--
-- Table structure for table `study1`
--

CREATE TABLE IF NOT EXISTS `study1` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cor` varchar(150) NOT NULL,
  `dep` varchar(150) NOT NULL,
  `sem` varchar(150) NOT NULL,
  `nme` varchar(150) NOT NULL,
  `des` text NOT NULL,
  `prc` varchar(150) NOT NULL,
  `ph` varchar(150) NOT NULL,
  `uid` varchar(150) NOT NULL,
  `st` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `study1`
--

INSERT INTO `study1` (`id`, `cor`, `dep`, `sem`, `nme`, `des`, `prc`, `ph`, `uid`, `st`) VALUES
(1, '2', '1', '2', 'Java Programing', 'Study guides can be broad based to facilitate learning in a number of areas, or be resources that foster comprehension of literature, research topics, history, and other subjects.\r\n\r\nGeneral topics include study and testing strategies; reading, writing, classroom, and project management skills; as well as techniques for learning as an adult, with disabilities, and online. Some will summarize chapters of novels or the important elements of the subject. Study guides for math and science often present problems (as in problem-based learning) and will offer techniques of resolution', '1000', '7552520.jpg', 'ma@gmail.com', 0),
(2, '2', '1', '2', 'Python Programing', 'Study guides can be broad based to facilitate learning in a number of areas, or be resources that foster comprehension of literature, research topics, history, and other subjects.\r\n\r\nGeneral topics include study and testing strategies; reading, writing, classroom, and project management skills; as well as techniques for learning as an adult, with disabilities, and online. Some will summarize chapters of novels or the important elements of the subject. Study guides for math and science often present problems (as in problem-based learning) and will offer techniques of resolution', '600', '6349792.jpg', 'ma@gmail.com', 0),
(3, '2', '1', '2', 'Networking ', 'Study guides can be broad based to facilitate learning in a number of areas, or be resources that foster comprehension of literature, research topics, history, and other subjects.\r\n\r\nGeneral topics include study and testing strategies; reading, writing, classroom, and project management skills; as well as techniques for learning as an adult, with disabilities, and online. Some will summarize chapters of novels or the important elements of the subject. Study guides for math and science often present problems (as in problem-based learning) and will offer techniques of resolution', '1200', '7342407.jpg', 'ma@gmail.com', 0),
(4, '2', '1', '2', 'Ethical Hacking', 'Study guides can be broad based to facilitate learning in a number of areas, or be resources that foster comprehension of literature, research topics, history, and other subjects.\r\n\r\nGeneral topics include study and testing strategies; reading, writing, classroom, and project management skills; as well as techniques for learning as an adult, with disabilities, and online. Some will summarize chapters of novels or the important elements of the subject. Study guides for math and science often present problems (as in problem-based learning) and will offer techniques of resolution', '5000', '666666.jpg', 'ma@gmail.com', 0);

-- --------------------------------------------------------

--
-- Table structure for table `study2`
--

CREATE TABLE IF NOT EXISTS `study2` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nme` varchar(150) NOT NULL,
  `des` text NOT NULL,
  `prc` varchar(150) NOT NULL,
  `ph` varchar(150) NOT NULL,
  `uid` varchar(150) NOT NULL,
  `st` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `study2`
--

INSERT INTO `study2` (`id`, `nme`, `des`, `prc`, `ph`, `uid`, `st`) VALUES
(1, 'Geometry box', 'Study guides can be broad based to facilitate learning in a number of areas, or be resources that foster comprehension of literature, research topics, history, and other subjects.\r\n\r\nGeneral topics include study and testing strategies; reading, writing, classroom, and project management skills; as well as techniques for learning as an adult, with disabilities, and online. Some will summarize chapters of novels or the important elements of the subject. Study guides for math and science often present problems (as in problem-based learning) and will offer techniques of resolution.', '200', '6174285.jpg', 'ma@gmail.com', 0),
(2, 'T-Scale', 'Study guides can be broad based to facilitate learning in a number of areas, or be resources that foster comprehension of literature, research topics, history, and other subjects.\r\n\r\nGeneral topics include study and testing strategies; reading, writing, classroom, and project management skills; as well as techniques for learning as an adult, with disabilities, and online. Some will summarize chapters of novels or the important elements of the subject. Study guides for math and science often present problems (as in problem-based learning) and will offer techniques of resolution.', '500', '1528442.jpg', 'ma@gmail.com', 0),
(3, 'Calculator', 'Study guides can be broad based to facilitate learning in a number of areas, or be resources that foster comprehension of literature, research topics, history, and other subjects.\r\n\r\nGeneral topics include study and testing strategies; reading, writing, classroom, and project management skills; as well as techniques for learning as an adult, with disabilities, and online. Some will summarize chapters of novels or the important elements of the subject. Study guides for math and science often present problems (as in problem-based learning) and will offer techniques of resolution.', '150', '7596740.jpg', 'ma@gmail.com', 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_data`
--

CREATE TABLE IF NOT EXISTS `user_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nme` varchar(150) NOT NULL,
  `cont` varchar(150) NOT NULL,
  `addr` text NOT NULL,
  `em` varchar(150) NOT NULL,
  `pas` varchar(150) NOT NULL,
  `ph` varchar(150) NOT NULL,
  `st` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `user_data`
--

INSERT INTO `user_data` (`id`, `nme`, `cont`, `addr`, `em`, `pas`, `ph`, `st`) VALUES
(1, 'Manu Krishna', '7985463215', 'Nalanchira Trivandrum Kerala', 'ma@gmail.com', '123', '1090911.jpg', 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_log`
--

CREATE TABLE IF NOT EXISTS `user_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` varchar(50) NOT NULL,
  `pwd` varchar(50) NOT NULL,
  `utyp` varchar(15) NOT NULL,
  `st` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `user_log`
--

INSERT INTO `user_log` (`id`, `uid`, `pwd`, `utyp`, `st`) VALUES
(1, 'admin@gmail.com', 'admin', 'admin', 1),
(2, 'ma@gmail.com', '123', 'usr', 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
